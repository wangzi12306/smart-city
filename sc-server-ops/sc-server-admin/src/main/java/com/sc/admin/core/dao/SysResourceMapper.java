package com.sc.admin.core.dao;

import com.sc.admin.dto.ResourceTreeDto;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.resource.SysResource;
import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DataAccessException;
import java.util.List;

public interface SysResourceMapper extends IBaseMapper<SysResource> {
	List<SysResource> findAnonResourcesByMenuId(String menuCode) throws DataAccessException;

	/**
	 * 获取指定账号拥有的非白名单资源
	 * @param permissionType
	 * @param lang
	 * @param accountId
	 * @return
	 */
	List<ResourceTreeDto> findByAccountId(@Param("permissionType")String permissionType, @Param("lang")String lang, @Param("accountId")Long accountId);

	/**
	 * 获取组织架构上指定岗位拥有的非白名单资源
	 * @param permissionType
	 * @param lang
	 * @param organizationId
	 * @return
	 */
	List<ResourceTreeDto> findByOrganizationId(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("organizationId")Long organizationId);

	int deleteAll() throws DataAccessException;


	/**
	 * 超级管理员：获取非白名单资源权限
	 * @return
	 */
	List<SysResource> findAllResources4superAdmin(@Param("permissionType")String permissionType,@Param("lang")String lang) throws DataAccessException;


	/**
	 * 超级管理员：获取白名单资源权限
	 * @return
	 */
	List<SysResource> findAllAnonResources4superAdmin(@Param("permissionType")String permissionType,@Param("lang")String lang) throws DataAccessException;


	/**
	 * 普通管理员：获取非白名单资源权限
	 * @param permissionType
	 * @param lang
	 * @param accountId
	 * @return
	 * @throws DataAccessException
	 */
	List<SysResource> findResources4admin(@Param("permissionType")String permissionType,@Param("lang")String lang,@Param("accountId") Long accountId) throws DataAccessException;


	/**
	 * 普通管理员：白名单资源权限
	 * @param permissionType
	 * @param lang
	 * @param accountId
	 * @return
	 * @throws DataAccessException
	 */
	List<SysResource> findAnonResources4admin(@Param("permissionType")String permissionType,@Param("lang")String lang,@Param("accountId") Long accountId) throws DataAccessException;


	/**
	 * 员工：非白名单资源权限
	 * @param userId
	 * @return
	 */
	List<SysResource> findResourcesByUserId(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("userId")Long userId) throws DataAccessException;


	/**
	 * 员工：白名单资源权限
	 * @param userId
	 * @return
	 */
	List<SysResource> findAnonResourcesByUserId(@Param("permissionType")String permissionType,@Param("lang")String lang, @Param("userId")Long userId) throws DataAccessException;
}
