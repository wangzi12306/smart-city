/**
 * Created by wust on 2019-11-06 15:11:11
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.feign;

import com.sc.admin.api.ProjectService;
import com.sc.admin.core.dao.SysProjectMapper;
import com.sc.common.annotations.FeignApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.project.SysProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: wust
 * @date: Created in 2019-11-06 15:11:11
 * @description:
 *
 */
@FeignApi
@RestController
public class ProjectFeignApi implements ProjectService {
    @Autowired
    private SysProjectMapper sysProjectMapper;



    @Override
    @RequestMapping(value = SELECT,method= RequestMethod.POST)
    public WebResponseDto select(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysProject search) {
        WebResponseDto responseDto = new WebResponseDto();
        List<SysProject> projects = sysProjectMapper.select(search);
        responseDto.setLstDto(projects);
        return responseDto;
    }
}
