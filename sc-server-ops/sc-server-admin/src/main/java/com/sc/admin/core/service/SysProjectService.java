package com.sc.admin.core.service;

import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.service.BaseService;

public interface SysProjectService extends BaseService<SysProject> {
}
