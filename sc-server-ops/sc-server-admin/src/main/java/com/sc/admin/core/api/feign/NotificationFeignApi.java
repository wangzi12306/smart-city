/**
 * Created by wust on 2019-11-09 11:16:52
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.feign;

import com.sc.admin.api.NotificationService;
import com.sc.admin.core.service.SysNotificationService;
import com.sc.common.annotations.FeignApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.notification.SysNotification;
import com.sc.common.util.MyIdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: wust
 * @date: Created in 2019-11-09 11:16:52
 * @description:
 *
 */
@FeignApi
@RestController
public class NotificationFeignApi implements NotificationService {
    @Autowired
    private SysNotificationService sysNotificationServiceImpl;


    @Override
    @RequestMapping(value = CREATE,method= RequestMethod.POST)
    public WebResponseDto create(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysNotification entity) {
        WebResponseDto responseDto = new WebResponseDto();
        entity.setId(MyIdUtil.getId());
        sysNotificationServiceImpl.insert(entity);
        responseDto.setObj(entity.getId());
        return responseDto;
    }
}
