package com.sc.admin.core.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.service.BaseService;

/**
 * @author: wust
 * @date: 2019-12-27 11:55:31
 * @description:
 */
public interface SysAccountService extends BaseService<SysAccount>{
    WebResponseDto setFunctionPermissions(JSONObject jsonObject);
}
