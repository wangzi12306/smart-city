package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysDataSourceDetailMapper;
import com.sc.admin.core.service.SysDataSourceDetailService;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.datasource.detail.SysDataSourceDetail;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-05-01 20:32:48
 * @description:
 */
@Service("sysDataSourceDetailServiceImpl")
public class SysDataSourceDetailServiceImpl extends BaseServiceImpl<SysDataSourceDetail> implements SysDataSourceDetailService {
    @Autowired
    private SysDataSourceDetailMapper sysDataSourceDetailMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysDataSourceDetailMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
