package com.sc.admin.core.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.service.BaseService;
import com.sc.common.entity.admin.notification.SysNotificationTimeline;

/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
public interface SysNotificationTimelineService extends BaseService<SysNotificationTimeline>{
    WebResponseDto createByMq(JSONObject jsonObject);
}
