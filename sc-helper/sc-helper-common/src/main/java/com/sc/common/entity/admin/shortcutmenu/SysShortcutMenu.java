package com.sc.common.entity.admin.shortcutmenu;

import com.sc.common.entity.BaseEntity;

/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
public class SysShortcutMenu extends BaseEntity {
    private static final long serialVersionUID = -3991719604296277320L;
    private String menuCode;
    private String name;
    private Long userId;
    private String description;

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}