/**
 * Created by wust on 2019-10-21 14:26:51
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.entity.admin.mynotice;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:26:51
 * @description:
 *
 */
public class SysMyNoticeList extends SysMyNotice {
    private static final long serialVersionUID = -2092810303441657227L;

    private String title;

    private String content;

    private String priorityLevel;

    private String priorityLevelLabel;

    private String statusLabel;

    private String type;

    private String typeLabel;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public String getPriorityLevelLabel() {
        return priorityLevelLabel;
    }

    public void setPriorityLevelLabel(String priorityLevelLabel) {
        this.priorityLevelLabel = priorityLevelLabel;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }
}
