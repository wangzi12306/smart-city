package com.sc.common.entity.admin.userorganization;

import com.sc.common.dto.PageDto;

/**
 * @author ：wust
 * @date ：Created in 2019/8/9 9:57
 * @description：
 * @version:
 */
public class SysUserOrganizationSearch extends SysUserOrganization {
    private static final long serialVersionUID = -1222205073903300868L;

    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }

}
