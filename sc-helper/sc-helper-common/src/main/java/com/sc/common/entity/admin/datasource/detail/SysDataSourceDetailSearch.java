package com.sc.common.entity.admin.datasource.detail;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-04-13 16:23:14
 * @description:
 */
public class SysDataSourceDetailSearch extends SysDataSourceDetail {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}