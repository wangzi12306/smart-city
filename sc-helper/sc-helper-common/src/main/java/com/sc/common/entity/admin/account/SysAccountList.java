package com.sc.common.entity.admin.account;


/**
 * @author: wust
 * @date: 2019-12-27 11:55:31
 * @description:
 */
public class SysAccountList extends SysAccount {
    private String typeLabel;

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }
}