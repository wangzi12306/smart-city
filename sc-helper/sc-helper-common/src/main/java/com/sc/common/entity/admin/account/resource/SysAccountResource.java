package com.sc.common.entity.admin.account.resource;


import com.sc.common.entity.BaseEntity;

/**
 * Created by wust on 2019/4/28.
 */
public class SysAccountResource extends BaseEntity {
    private static final long serialVersionUID = 4293170125689800016L;
    private Long accountId;
    private String resourceCode;	    // 资源code, 当srcType为m时,为菜单code,为r时,指向资源code
    private String type;		        // 资源类型,m为菜单,r为资源


    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
