package com.sc.common.entity.admin.datasource;

/**
 * Created by wust on 2019/6/17.
 */
public class SysDataSourceList extends SysDataSource {
    private static final long serialVersionUID = 7095378836627433753L;

    private String agentName;

    private String projectName;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
