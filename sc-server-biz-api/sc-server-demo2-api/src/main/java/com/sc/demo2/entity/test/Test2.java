package com.sc.demo2.entity.test;

import com.sc.common.entity.BaseEntity;

import javax.persistence.Table;

/**
 * @author: wust
 * @date: 2020-07-04 13:29:42
 * @description:
 */
@Table(name = "test")
public class Test2 extends BaseEntity {
        private String name;
    private String value;
    private Long projectId;
    private Long companyId;
                        

        public void setName (String name) {this.name = name;} 
    public String getName(){ return name;} 
    public void setValue (String value) {this.value = value;} 
    public String getValue(){ return value;} 
    public void setProjectId (Long projectId) {this.projectId = projectId;} 
    public Long getProjectId(){ return projectId;} 
    public void setCompanyId (Long companyId) {this.companyId = companyId;} 
    public Long getCompanyId(){ return companyId;} 
                        
}