package com.sc.workorder.core.service;

import com.sc.common.service.BaseService;
import com.sc.workorder.entity.WoWorkOrderTimeline;

/**
 * @author: wust
 * @date: 2020-04-09 13:13:36
 * @description:
 */
public interface WoWorkOrderTimelineService extends BaseService<WoWorkOrderTimeline>{
}
