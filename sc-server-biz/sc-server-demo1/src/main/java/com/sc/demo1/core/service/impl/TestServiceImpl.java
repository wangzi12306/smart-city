package com.sc.demo1.core.service.impl;

import com.sc.demo1.core.dao.TestMapper;
import com.sc.demo1.core.service.TestService;
import com.sc.demo1.entity.test.Test1;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-07-04 13:40:17
 * @description:
 */
@Service("testServiceImpl")
public class TestServiceImpl extends BaseServiceImpl<Test1> implements TestService{
    @Autowired
    private TestMapper testMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return testMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
