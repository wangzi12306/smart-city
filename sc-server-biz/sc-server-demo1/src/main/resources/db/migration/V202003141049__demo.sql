
CREATE TABLE IF NOT EXISTS `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `script` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`) USING BTREE,
  KEY `flyway_schema_history_s_idx` (`success`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;


CREATE TABLE IF NOT EXISTS `demo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网关名称',
  `mac` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '物理地址',
  `image` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备图片',
  `ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备ip',
  `sn` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备序列号',
  `type` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网关类型',
  `software_version` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '软件版本号',
  `hardware_version` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '硬件版本号',
  `status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网关类型',
  `area_id` bigint(20) DEFAULT NULL COMMENT '区域id',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备描述',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备自动定位地址：也可以参考配电结构的安装地址',
  `point_x` decimal(11,8) DEFAULT NULL,
  `point_y` decimal(11,8) DEFAULT NULL,
  `last_connection_time` datetime DEFAULT NULL COMMENT '最后上线时间',
  `last_disconnection_time` datetime DEFAULT NULL COMMENT '最后离线时间',
  `project_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20) DEFAULT NULL,
  `creater_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '非必填',
  `modify_id` bigint(20) DEFAULT NULL,
  `modify_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_id` (`id`) USING BTREE,
  UNIQUE KEY `idx_unique_mac` (`mac`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1242722615131373569 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;


CREATE TABLE IF NOT EXISTS `test` (
	`id` BIGINT NOT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`value` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`project_id` BIGINT NULL DEFAULT NULL,
	`company_id` BIGINT NULL DEFAULT NULL,
	`creater_id` BIGINT NULL DEFAULT NULL,
	`creater_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`create_time` DATETIME NULL DEFAULT NULL COMMENT '非必填',
	`modify_id` BIGINT NULL DEFAULT NULL,
	`modify_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`modify_time` DATETIME NULL DEFAULT NULL,
	`is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
;
