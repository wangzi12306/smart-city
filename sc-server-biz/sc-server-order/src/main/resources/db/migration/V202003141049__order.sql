
CREATE TABLE IF NOT EXISTS `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `script` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`) USING BTREE,
  KEY `flyway_schema_history_s_idx` (`success`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;



CREATE TABLE IF NOT EXISTS `t_goods`  (
  `id` bigint(20)  NOT NULL,
  `goods_no` varchar(35)  NULL DEFAULT NULL COMMENT '商品编号',
  `name` varchar(35)  NULL DEFAULT NULL COMMENT '商品名称',
  `unit_price` DECIMAL(11,3) NULL DEFAULT NULL COMMENT '商品单价',
  `picture` bigint(20)  NULL DEFAULT NULL COMMENT '商品图片',
  `stock` bigint NULL DEFAULT NULL COMMENT '库存',
  `description` varchar(200)  NULL DEFAULT NULL COMMENT '描述',
  `project_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20)  NULL DEFAULT NULL COMMENT '创建用户id',
  `creater_name` varchar(50)  NULL DEFAULT NULL COMMENT '创建用户名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20)  NULL DEFAULT NULL COMMENT '修改人id',
  `modify_name` varchar(50)  NULL DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '商品表';


CREATE TABLE IF NOT EXISTS `t_order`  (
  `id` bigint(20)  NOT NULL,
  `order_no` varchar(20)  NULL DEFAULT NULL COMMENT '订单号',
  `buyer_id` bigint(20)  NULL DEFAULT NULL COMMENT '买家ID',
  `buyer_name` varchar(100)  NULL DEFAULT NULL COMMENT '买家名字',
  `buyer_telephone` varchar(100)  NULL DEFAULT NULL COMMENT '买家电话',
  `buyer_address` varchar(100)  NULL DEFAULT NULL COMMENT '买家地址',
  `total_amount` DECIMAL(11,3) NULL DEFAULT NULL COMMENT '订单总金额',
  `status` varchar(15)  NULL DEFAULT NULL COMMENT '订单状态',
  `pay_type` varchar(15)  NULL DEFAULT NULL COMMENT '支付类型：现金、刷卡、微信支付宝',
  `pay_status` varchar(15)  NULL DEFAULT NULL COMMENT '支付状态：成功、失败',
  `business_serial_number` varchar(50)  NULL DEFAULT NULL COMMENT '交易流水号',
  `project_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20)  NULL DEFAULT NULL COMMENT '创建用户id',
  `creater_name` varchar(50)  NULL DEFAULT NULL COMMENT '创建用户名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20)  NULL DEFAULT NULL COMMENT '修改人id',
  `modify_name` varchar(50)  NULL DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '订单主表';


CREATE TABLE IF NOT EXISTS `t_order_detail`  (
  `id` bigint(20)  NOT NULL,
  `order_id` bigint(20)  NULL DEFAULT NULL COMMENT '订单ID',
  `order_no` varchar(20)  NULL DEFAULT NULL COMMENT '订单号',
  `quantity` tinyint(4) NULL DEFAULT NULL COMMENT '购买数量',
  `goods_id` bigint(20)  NULL DEFAULT NULL COMMENT '商品ID',
  `goods_name` varchar(50)  NULL DEFAULT NULL COMMENT '商品名称',
  `goods_price` DECIMAL(11,3) NULL DEFAULT NULL COMMENT '商品单价',
  `goods_picture` bigint(20)  NULL DEFAULT NULL COMMENT '商品图片',
  `total_amount` DECIMAL(11,3) NULL DEFAULT NULL COMMENT '订单金额',
  `description` varchar(200)  NULL DEFAULT NULL COMMENT '描述',
  `project_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `creater_id` bigint(20)  NULL DEFAULT NULL COMMENT '创建用户id',
  `creater_name` varchar(50)  NULL DEFAULT NULL COMMENT '创建用户名称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modify_id` bigint(20)  NULL DEFAULT NULL COMMENT '修改人id',
  `modify_name` varchar(50)  NULL DEFAULT NULL COMMENT '修改人名称',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` INT NOT NULL DEFAULT 0 COMMENT '是否已经删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB COMMENT = '订单明细';

