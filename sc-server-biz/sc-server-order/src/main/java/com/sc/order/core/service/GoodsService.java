package com.sc.order.core.service;

import com.sc.common.service.BaseService;
import com.sc.order.entity.goods.Goods;

/**
 * @author: wust
 * @date: 2020-07-15 13:54:05
 * @description:
 */
public interface GoodsService extends BaseService<Goods>{
}
