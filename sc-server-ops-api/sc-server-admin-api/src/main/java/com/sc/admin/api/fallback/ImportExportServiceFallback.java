package com.sc.admin.api.fallback;

import com.sc.admin.api.ImportExportService;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import org.springframework.stereotype.Component;

/**
 * 服务降级配置
 */
@Component
public class ImportExportServiceFallback implements ImportExportService {
    @Override
    public WebResponseDto insert(String ctx, SysImportExport entity) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }

    @Override
    public WebResponseDto selectOne(String ctx, SysImportExport search) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }

    @Override
    public WebResponseDto updateByPrimaryKeySelective(String ctx, SysImportExport entity) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }
}
