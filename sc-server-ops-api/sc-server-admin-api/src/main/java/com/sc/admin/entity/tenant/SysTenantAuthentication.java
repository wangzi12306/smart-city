package com.sc.admin.entity.tenant;


import com.sc.common.entity.BaseEntity;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 10:21:44
 * @description:
 */
@ApiModel(description = "实体对象-SysTenantAuthentication")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class SysTenantAuthentication extends BaseEntity {
    private Long tenantId;
    private String corporateRepresentative;
    private String corporateRepresentativeIdNumber;
    private Long corporateRepresentativeIdPicture1;
    private Long corporateRepresentativeIdPicture2;
    private String businessLicenceNumber;
    private Long businessLicencePicture1;
    private Long businessLicencePicture2;
    private String status;
    private String rejectReason;
    private Integer isDeleted;

}